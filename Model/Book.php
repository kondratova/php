<?php
/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 26.05.2018
 * Time: 9:41
 */

class Book extends AbstractModel
{
    protected static $table = 'books';

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $author;

    /**
     * @var integer
     */
    public $year;

    /**
     * @var integer
     */
    public $user_id;
}
<?php

/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 25.05.2018
 * Time: 22:52
 */
class RegisterController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->render('index');
    }

    public function loginAction()
    {
        return $this->render('login');
    }

    public function authAction(Request $request)
    {
        $model = User::findOneBy(['email' => $request->post('email')]);

        if ($model) {
            if (md5($request->post('password')) === $model->password) {
                $_SESSION['user_id'] = $model->id;
            } else {
                var_dump("password is incorrect");
                die();
            }
        } else {
            var_dump('User with requested email was not found in app');
            die();
        }

        return Router::redirect('/');
    }

    /**
     * @param Request $request
     */
    public function storeAction(Request $request)
    {
        $model = new User();
        $model->email = $request->post('email');
        $model->password = md5($request->post('password'));
        $model->name = $request->post('name');
        $model->ip = $_SERVER['REMOTE_ADDR'];

        $model->save();

        return Router::redirect('/');
    }

    public function editAction(Request $request)
    {
        $model = User::find($request->get('id'));

        return $this->render('edit', compact('model'));
    }

    public function updateAction(Request $request)
    {
        /**
         * @var User $model
         */
        $model = User::find($request->get('id'));

        $model->name = $request->post('name');
        $model->email = $request->post('email');

        $model->save();
    }


}
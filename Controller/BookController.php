<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 24.04.18
 */

class BookController extends Controller
{
    public function createAction(Request $request)
    {
        return $this->render('create');
    }


    public function storeAction(Request $request)
    {
        if (!$request->isPost()) {
            return false;
        }

        $model = new Book();
        $model->title = $request->post('title');
        $model->author = $request->post('author');
        $model->year = $request->post('year');
        $model->user_id = $_SESSION['user_id'];
        $model->save();

        $_SESSION['status'] = 'Book ' . $request->post('title') . ' has been successfully stored';

        return header('Location: ?route=book/index');
    }

    public function indexAction()
    {
        $status = isset($_SESSION['status']) ? $_SESSION['status'] : null;

        unset($_SESSION['status']);

        return $this->render('index', [
            'books' => Book::findAllBy(['user_id' => $_SESSION['user_id']]),
            'status' => $status
        ]);
    }

    public function searchAction(Request $request)
    {
        $model = Book::findAllBy(['user_id' => $_SESSION['user_id']]);
        while ($row = $model->fetch_assoc()) {
            if ($row['title'] == $request->get('title')) {
                $book = $row;
            }
        }

        if (empty($book)) {
            $_SESSION['status'] = "the book was not found";
        }

        $status = isset($_SESSION['status']) ? $_SESSION['status'] : null;

        unset($_SESSION['status']);

        return $this->render('search', [
            'book' => $book,
            'status' => $status
        ]);


    }
}

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div>
    <?php
    /**
     * Created by PhpStorm.
     * User: Vika
     * Date: 21.04.2018
     * Time: 10:23
     */
    $mysqli = new mysqli('127.0.0.1', 'root', '', 'Kondratova');
    if ($mysqli->connect_errno) {
        echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }
    $result = $mysqli->query("SELECT * FROM `books`");
    echo "<table align=center width=70%>";
    echo "<caption><strong>Cписок книг</strong></caption>";
    echo "<tr>";
    echo "<td><strong>Название</strong></td>";
    echo "<td><strong>Год выпуска</strong></td>";
    echo "<td><strong>Автор</strong></td>";
    echo "</tr>";
    for ($i = $result->num_rows - 1; $i >= 0; $i--) {
        $result->data_seek($i);
        $row = $result->fetch_assoc();
        echo "<tr>";
        echo "<td>" . $row['title'] . "</td>";
        echo "<td>" . $row['year'] . "</td>";
        echo "<td>" . $row['author'] . "</td>";
        echo "</tr>";
    }
    ?>
</div>
<form>
   <p> <a href="show.php?id=2">Show</a>

    <a href="delete.php?id=2">Delete</a>

    <a href="add.php">Add</a></p>
</form>
</body>
</html>

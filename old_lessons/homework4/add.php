<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p><a href="index.php">На главную</a></p>
<form action="store.php" method="post" enctype="multipart/form-data">
    <label for="title">Title</label>
    <input type="text" name="title">

    <label for="year">Year</label>
    <input type="number" min="1700" max="2018" name="year">

    <label for="author">Author</label>
    <input type="text" name="author">

    <input type="file" name="file" required>
    <button type="submit">
        Upload file
    </button>
</form>
</body>
</html>
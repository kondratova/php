<?php
/**
 * @param $value
 */
function factorial($value)
{
    $factorial = 1;
    for ($i = 1; $i <= $value; $i++) {
        $factorial = $factorial * $i;
    }
    return $factorial;
}

$value = 3;
echo factorial($value);
/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 01.04.2018
 * Time: 0:09
 */
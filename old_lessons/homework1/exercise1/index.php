<?php
function arrayScan($array)
{
    $arrayModified = [];
    foreach ($array as $value) {
        if (is_int($value)) {
            if ($value % 5 == 0) {
                $arrayModified[] = $value;
            }
        }

    };
    return $arrayModified;
}

$array = [2, 5, 3, 10, 34];
print_r(arrayScan($array));
/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 31.03.2018
 * Time: 21:58
 */
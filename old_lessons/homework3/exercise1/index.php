<?php

class WorkerAlpha
{
    public $age;
    public $name;
    public $salary;
}

$firstWorker = new WorkerAlpha();
$firstWorker->name = 'Ivan';
$firstWorker->age = 25;
$firstWorker->salary = 1000;
$secondWorker = new WorkerAlpha();
$secondWorker->name = 'Vasya';
$secondWorker->age = 26;
$secondWorker->salary = 2000;
echo 'WorkerAlpha';
echo '<br>';
echo $firstWorker->age + $secondWorker->age;
echo '<br>';
echo $firstWorker->salary + $secondWorker->salary;
echo '<br>';

class WorkerBeta
{
    private $name;
    private $age;
    private $salary;

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }

}

$firstWorker = new WorkerBeta();
$firstWorker->setName('Ivan');
$firstWorker->setAge(25);
$firstWorker->setSalary(1000);
$secondWorker = new WorkerBeta();
$secondWorker->setName('Vasya');
$secondWorker->setAge(26);
$secondWorker->setSalary(2000);
echo 'WorkerBeta';
echo '<br>';
echo $firstWorker->getAge() + $secondWorker->getAge();
echo '<br>';
echo $firstWorker->getSalary() + $secondWorker->getSalary();
echo '<br>';

class WorkerOmega
{
    private $name;
    private $age;
    private $salary;

    private function checkAge($age)
    {
        if ($age > 1 and $age < 100) {
            return true;
        }
        return false;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        if($this->checkAge($age)){
            $this->age = $age;
        }
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }
}

$firstWorker = new WorkerOmega();
$firstWorker->setName('Ivan');
$firstWorker->setAge(25);
$firstWorker->setSalary(1000);
$secondWorker = new WorkerOmega();
$secondWorker->setName('Vasya');
$secondWorker->setAge(26);
$secondWorker->setSalary(2000);
echo 'WorkerOmega';
echo '<br>';
echo $firstWorker->getAge() + $secondWorker->getAge();
echo '<br>';
echo $firstWorker->getSalary() + $secondWorker->getSalary();
/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 15.04.2018
 * Time: 20:16
 */
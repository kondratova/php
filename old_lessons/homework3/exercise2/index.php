<?php

class  MyCalculator
{
    private $a;
    private $b;

    public function __construct($a, $b)
    {
        $this->a = $a;
        $this->b = $b;
    }

    public function add()
    {
        return $this->a + $this->b;
    }

    public function multiply()
    {
        return $this->a * $this->b;
    }

    public function subtraction()
    {
        return $this->a - $this->b;
    }

    public function divide()
    {
        return $this->a / $this->b;
    }
}

$mycalc = new MyCalculator(12, 6);
echo $mycalc->add(); // Displays 18
echo "<br>";
echo $mycalc->multiply(); // Displays 72
echo "<br>";
echo $mycalc->subtraction();
echo "<br>";
echo $mycalc->divide();

/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 15.04.2018
 * Time: 14:06
 */
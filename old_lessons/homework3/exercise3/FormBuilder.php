<?php

class  FormBuilder
{
    public function open($action, $method)
    {
        return "<form action=\"$action\" method=\"$method\">";
    }

    public function input($type, $placeholder, $name)
    {
        return "<input type=\"$type\" placeholder=\"$placeholder\" name=\"$name\">";
    }

    public function submit($value)
    {
        return "<button type=”submit”>$value</button>";
    }

    public function close()
    {
        return "</form>";
    }
}


/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 15.04.2018
 * Time: 13:21
 */
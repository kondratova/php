<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 10.04.18
 */
define('ALLOWED_FORMATS', ['xml', 'json', 'png', 'xls']);

$fileName = $_POST['name'];
$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

if (!in_array($extension, ALLOWED_FORMATS)) {
    $formats = convertAllowedFormats();
    echo "Invalid format. Only {$formats} are supported";
} else {
    var_dump(file_put_contents($fileName . '.' . $extension, file_get_contents($_FILES['file']['tmp_name'])));
}

/**
 * @return string
 */
function convertAllowedFormats() {
    $string = '';
    foreach (ALLOWED_FORMATS as $format) {
        $string .= ".{$format}, ";
    }

    return $string;
}

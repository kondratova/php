<?php

/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 25.05.2018
 * Time: 22:27
 */
abstract class AbstractModel
{
    protected static $table = '';

    public static function all()
    {
        $table = static::$table;
        return self::getConnection()->query("SELECT * FROM {$table}");
    }

    /**
     * @return mysqli
     */
    private static function getConnection()
    {
        return DbConnection::getInstance()->getMysqli();
    }

    /**
     * @param int $id
     * @return AbstractModel
     */
    public static function find($id)
    {
        $table = static::$table;

        $entity = self::getConnection()->query("SELECT * FROM {$table} WHERE `id` = {$id}")->fetch_assoc();

        $model = new static();

        foreach ($entity as $fieldName => $value) {
            $model->$fieldName = $value;
        }

        return $model;
    }

    public static function last()
    {
        return self::getConnection()->query("SELECT * FROM {static::$table} ORDER BY `id` DESC LIMIT 1");
    }

    public function save()
    {
        if ($this->id) {
            return $this->update();
        } else {
            return $this->insert();
        }


    }

    protected function update()
    {
        $table = static::$table;

        $values = [];

        foreach (get_object_vars($this) as $propertyName => $value) {
            if ($propertyName === 'id') {
                continue;
            }
            $values[] = "`" . $propertyName .  "`" . '=' . "'" . $value . "'";
        }

        $values = implode(',', $values);

        return self::getConnection()->query("UPDATE `{$table}` SET {$values} WHERE `id` = {$this->id}");
    }

    public static function findOneBy(array $condition)
    {
        $table = static::$table;

        $field = array_keys($condition)[0];

        $value = array_values($condition)[0];

        $entity = self::getConnection()
            ->query("SELECT * FROM {$table} WHERE `{$field}` = '{$value}' LIMIT 1")->fetch_assoc();


        if (empty($entity)) {
            return false;
        }

        $model = new static();

        foreach ($entity as $fieldName => $value) {
            $model->$fieldName = $value;
        }

        return $model;
    }

    protected function insert()
    {
        $table = static::$table;

        $fields = [];
        $values = [];

        foreach (get_object_vars($this) as $propertyName => $value) {
            if ($propertyName === 'id') {
                continue;
            }
            $fields[] = "`" . $propertyName . "`";
            $values[] = "'" . $value . "'";
        }

        $fields = implode(', ', $fields);
        $values = implode(', ', $values);

        return self::getConnection()->query("INSERT INTO `{$table}` ({$fields}) VALUES ($values)");
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 07.05.2018
 * Time: 19:58
 */

class CurrencyController extends Controller
{
    const API = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';

    public function indexAction()
    {
        $date = file_get_contents(self::API);
        $date = json_decode($date);
        $usd = $date[0];
        $eur = $date[1];
        $rur = $date[2];
        $btc = $date[3];

        return $this->render('index', [
            'usd' => $usd,
            'eur' => $eur,
            'rur' => $rur,
            'btc' => $btc
        ]);
    }


    public function calculateAction(Request $request)
    {
        if (!$request->isPost()) {
            return false;
        }

        $ccy1 = $request->post('currency1');
        $number = $request->post('number');
        $ccy2 = $request->post('currency2');

        $exchange = file_get_contents(self::API);
        $exchange = json_decode($exchange);

        foreach ($exchange as $obj) {
            if ($ccy1 == $obj->ccy) {
                $currency1 = $obj;
            }
            if ($ccy2 == $obj->ccy) {
                $currency2 = $obj;
            }
        }

        if ($ccy1 == 'UAH' && $ccy2 == 'UAH') {
            $count = $number;
        } elseif ($ccy2 == 'UAH') {
            $count = ($ccy1 == 'BTC') ? $number * $currency1->sale * $exchange[0]->sale : $number * $currency1->sale;
        } elseif ($ccy1 == 'UAH') {
            $count = ($ccy2 == 'BTC') ? $number / $currency2->sale / $exchange[0]->sale : $number / $currency2->sale;
        } elseif ($ccy1 == 'BTC') {
            $count = $number * $currency1->sale * $exchange[0]->sale / $currency2->sale;
        } else {
            $count = ($ccy2 == 'BTC') ? $number * $currency1->sale / $exchange[0]->sale / $currency2->sale
                : $number * $currency1->sale / $currency2->sale;
        }

        $count = round($count,2,PHP_ROUND_HALF_ODD);

        $_SESSION['result'] = $number . ' ' . $ccy1 . ' = ' . $count . ' ' . $ccy2;

        return header('Location: ?route=currency/converter');
    }

    public
    function converterAction()
    {
        $result = isset($_SESSION['result']) ? $_SESSION['result'] : null;

        unset($_SESSION['result']);

        return $this->render('converter', [
            'result' => $result
        ]);
    }
}
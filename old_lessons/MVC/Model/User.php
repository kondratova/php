<?php
/**
 * Created by PhpStorm.
 * User: Vika
 * Date: 26.05.2018
 * Time: 9:42
 */

class User extends AbstractModel
{
    protected static $table = 'users';

    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $ip;
}